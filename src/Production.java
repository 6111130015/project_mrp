public class Production {
    private String[] refmfg;
    private String[] pn;
    private int[] qtyord;
    private int[] qtyrcd;
    private int[] qtybo;
    private String[] duedate;
    private String[] ztype;

    public Production(String[] refmfg, String[] pn, int[] qtyord, int[] qtyrcd, int[] qtybo, String[] duedate, String[] ztype) {
        this.refmfg = refmfg;
        this.pn = pn;
        this.qtyord = qtyord;
        this.qtyrcd = qtyrcd;
        this.qtybo = qtybo;
        this.duedate = duedate;
        this.ztype = ztype;

        System.out.println("PRODUCTION");
        System.out.println("----------------------------------------------------------------------------------------");
        count();
    }

    public void count() {
        for (int i = 0; i < 10; i++) {
            System.out.println(refmfg[i] + " " + pn[i] + " " + qtyord[i] + " " + qtyrcd[i] + " " + qtybo[i] + " " + duedate[i] + " " + ztype[i]);
        }System.out.println("----------------------------------------------------------------------------------------");
    }
}

