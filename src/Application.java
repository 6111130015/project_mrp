public class Application {
    public static void main (String[] args) {

        Inv inv = new Inv(
                new String[]{"A1001","A1002","A1003","A1004","A1005","A1006","A1007","A1008","A1009","A1010"},
                new String[]{"Desc of Part A1009","Desc of Part A1010","Desc of Part B1001","Desc of Part B1002","Desc of Part B1003","Desc of Part B1004","Desc of Part B1005","Desc of Part C1001","Desc of Part C1002","Desc of Part C1003"},
                new String[]{"set","set","set","set","set","set","set","set","set","set"},
                new int[]{200,200,250,500,500,1000,1000,1000,2000,5000},
                new int[]{100,100,100,100,100,100,100,100,100,100},
                new int[]{100,100,100,100,100,100,100,100,100,100},
                new int[]{2000,2000,2000,2000,2000,2000,2000,2000,2000,2000},
                new String[]{"M","M","M","M","M","M","M","M","M","M"},
                new int[]{5,7,7,7,10,5,7,7,7,10},
                new int[]{80,120,125,350,145,150,200,165,160,520});

        Bom bom = new Bom(
                new String[]{"A1001","A1001","A1001","A1001","A1001","A1006","A1007","A1008","A1009","A1010","B1001","B1002","B1003","B1004","B1005","C1001","C1002","C1003","C1004","C1005"},
                new String[]{"A2001","A2002","A2003","A2004","A2005","A2006","A2007","A2008","A2009","A2010","B2001","B2002","B2003","B2004","B2005","C2001","C2002","C2003","C2004","C2005"},
                new int[]{1,2,4,5,5,2,2,2,2,2,5,5,5,5,5,1,1,1,1,1},
                new int[]{1,1,100,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2});

        SalesOrder salesOrder = new SalesOrder(
                new String[]{"A1001","A1003","A1007","A1008","B1001","B1002","B1003","C1001","C1002"},
                new int[]{2000,1000,2000,5000,2000,3000,2000,1000,2000,2000},
                new int[]{0,200,500,1000,1200,350,450,0,400,1000},
                new int[]{2000,800,1500,4000,800,2650,1550,1000,1600,1000},
                new String[]{"5/31/2021","5/31/2021","7-Apr","7-Apr","31-Mar","7-Apr","14-Apr","14-Apr","31-Mar","24-Mar"});

        Purchase purchase = new Purchase(
                new String[]{"PD0001","PD0002","PD0003","PD0004","PD0005","PD0006","PD0007"},
                new String[]{"A2002","A2003","A2004","A2003","A2004","A2004","A2005"},
                new int[]{2000,2000,2000,3000,3000,1000,1000},
                new int[]{200,1000,500,600,2000,0,200},
                new int[]{1800,1000,1500,2400,1000,1000,800},
                new String[]{"21-May","21-May","21-May","24-May","31-May","31-May","24-May"},
                new String[]{"PO","PR","PR","PO","PO","PR","PR"});

        Production production = new Production(
                new String[]{"PD0001","PD0002","PD0003","PD0004","PD0005","PD0006","PD0007","PD0008","PD0009","PD00010"},
                new String[]{"A1001","A1001","A1004","A1005","A1008","B1001","B1002","B1004","C1001","C1002"},
                new int[]{200,500,400,200,100,500,600,1000,1000,500},
                new int[]{50,100,200,100,0,200,0,200,400,100},
                new int[]{150,400,200,100,100,300,600,800,600,400},
                new String[]{"28-May","28-May","31-May","1-Apr","31-May","1-Apr","1-Apr","5-Apr","5-Apr","7-Apr"},
                new String[]{"OH","OH","OH","OH","OH","OH","OH","OH","OH","OH"});

    }
}
